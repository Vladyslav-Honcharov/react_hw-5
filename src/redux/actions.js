import {
  ADD_TO_CART,
  ADD_TO_FAVORITES,
  REMOVE_FROM_CART,
  REMOVE_FROM_FAVORITES,
  SET_DATA_LIST,
  OPEN_MODAL,
  CLOSE_MODAL,
  CLEAR_CART,
} from "./type";

export const addToCart = (item) => {
  return {
    type: ADD_TO_CART,
    payload: item,
  };
};

export const addToFavorites = (name, price, url, color) => ({
  type: ADD_TO_FAVORITES,
  payload: {
    name,
    price,
    url,
    color,
  },
});

export const removeFromCart = (index) => ({
  type: REMOVE_FROM_CART,
  payload: index,
});

export const removeFromFavorites = (index) => {
  return {
    type: REMOVE_FROM_FAVORITES,
    payload: index,
  };
};

export const setDataList = (dataList) => ({
  type: SET_DATA_LIST,
  payload: dataList,
});

export const openModal = (index) => ({
  type: OPEN_MODAL,
  payload: index,
});

export const closeModal = () => ({
  type: CLOSE_MODAL,
});

export const clearCart = (cartList) => ({
  type: CLEAR_CART,
  payload: cartList,
});
